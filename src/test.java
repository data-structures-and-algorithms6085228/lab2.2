
public class test {
    public static int removeElement(int[] nums, int val) {
        int k = 0; // ใช้เก็บจำนวนสมาชิกที่ไม่เท่ากับ val

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i]; // กำหนดค่าให้ nums ตามลำดับที่ไม่เท่ากับ val
                k++; // เพิ่มค่า k ขึ้นอีก 1
            }
        }

        return k; // คืนค่า k ที่เป็นจำนวนสมาชิกที่ไม่เท่ากับ val
    }

    public static void main(String[] args) {
        int[] nums = { 3, 2, 2, 3 };
        int val = 3;

        int result = removeElement(nums, val);

        System.out.println("Output: " + result); // Output: 2

        System.out.print("Modified nums: [");
        for (int i = 0; i < result; i++) {
            System.out.print(nums[i]);
            if (i < result - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

}