public class RemoveElement {
    public static void printArr(int[] nums, int r){
        System.out.print("[");
        for(int i=0;i<r;i++){
            System.out.print(nums[i]);
            if(i < r - 1){
                System.out.print(", ");
            }
        }
        System.out.println("]");
        
    }

    public static int removeElement(int[] nums, int val){
        int k = 0;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != val){
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) throws Exception {
       int nums1[] = {3,2,2,3};
       int val1 = 3;
       int r1 = removeElement(nums1, val1);

       System.out.print(r1 + ", ");
       System.out.print("nums = ");
       printArr(nums1, r1);
//--------------------------------------------------------------
       int nums2[] = {0,1,2,2,3,0,4,2};
       int val2 = 2;
       int r2 = removeElement(nums2, val2);
       System.out.print(r2 + ", ");
       System.out.print("nums = ");
       printArr(nums2, r2);
    }
}
